Introduction
============

The :mod:`apeman` package provides pythoneers with an elegant monkey patching mechanism.
The :mod:`overlays` package provides pythoneers with the authors overlays, that is a collection of patches.

:mod:`apeman` allows one to create overlays for common python libraries, that is patches that specialize their behaviour in some way.
The overlay(s) are then substituted for the original(s) by :class:`apeman.Apeman` when one performs their usual import(s).

This package provides the various overlays that the author would have included within :mod:`apeman` but decided were better off in a separate package.
This allows the user the choice of using either the authors overlays or their own overlays within their python projects.

This also serves as a suite of examples for those wishing to use :mod:`apeman` to implement their own overlays.

Setup
=====

Users may use Python's package installer pip to manage the installation, upgrade and removel of :mod:`overlays`.


Install
-------

The easiest means of installing the **Apeman:Overlay(s)** package is to invoke :command:`pip` as follows
::

  pip install apeman-overlays

Upgrade
-------

To upgrade the package invoke :command:`pip` with the :option:`--upgrade` command line switch as follows
::

  pip install --upgrade apeman-overlays

Remove
-------

One may remove the package by invoking :command:`pip` one last time with the :option:`uninstall` command as follows
::

  pip uninstall apeman-overlays

Usage
=====

Once :mod:`overlays` is installed one simply imports the module. 
The overlays are then applied during subsequent imports.
The following example illustrates the usage of the package.

.. code-block:: python
  :caption: PROJECT/PACKAGE/MODULE.py
  :name: use

   #!/usr/bin/python
   """
   Module
   ======
   
   Module documentation information ...
   """
   # Imports here import the original packages/modules
   import overlays
   # Imports here import the patched packages/modules
   ...
  
This effectively installs all of the overlays, that this package provides, in front of the packages, that they patch, upon the Python path.
This import must be performed before any other imports to ensure that they are caught and that the overlay(s) are properly substituted for the original modules.

.. note:: 
  
   When python starts up it imports a default suite of packages that may or may not have overlays.
   If,  subsequently, :mod:`overlays` is imported it cannot substitute these modules. 
   Supposedly one may disable these imports by using the :option:`-S` option when invoking Python,
   ::

     python -S
     
   and subsequently importing the :mod:`site` packages.
   ::

     import overlays
     import site

.. note:: 
  
   :mod:`overlays`, when used, should really be the first import performed in a project.
   This can make it's usage a little cumbersome in larger projects with multiple entry points and during module development.
   Ones recomendation is to place this `import` first within in a projects :file:`__init__.py` and :file:`__main__.py` files.
   Within other :file:`PROJECT/PACKAGE/MODULE.py` files one should additionally guard the import as follows.
   ::

     if __name__ == "__main__" :
      import overlays

   This will be necessary until :mod:`apeman` ensures :class:`Apeman` becomes a singleton and prevents duplicituous references to itself in :data:`sys.metapath`.
