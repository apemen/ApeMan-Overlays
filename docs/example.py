"""
The naming is a bit duplicitous but one should observe that this
script imports the underscored module from the overlay package.
That is given the following file structure 
 
 overlay\            The overlay package providing patched files
    __init__.py      The modified importer hooking in the patches
    _example.py      This is an example monkey patching module
    example.py       This is ignored by the __init__.py file
 example.py          The script making the calls to the overlays   

example.py will import the module overlay\_example_.py via the 
importer in overlay\__init__.py.
"""
from overlay import decimal
from decimal import Decimal
from pprint  import pprint

print(decimal.__file__)
print(decimal.Decimal)
print(Decimal)
print(Decimal(123))
