-----------------------
module Specification(s)
-----------------------

.. automodule:: overlays._unittest_._mock_.mock
  :members:         
  :undoc-members:   
  :show-inheritance:

.. Moved to Pathlib
.. .. automodule:: overlays._unittest_._mock_.module
..   :members:         
..   :undoc-members:   
..   :show-inheritance:

.. automodule:: overlays._unittest_._mock_.modname
  :members:         
  :undoc-members:   
  :show-inheritance:

.. automodule:: overlays._unittest_._mock_.modspec
  :members:         
  :undoc-members:   
  :show-inheritance:
