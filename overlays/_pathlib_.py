"""
Pathlib Overlay
===============

This extends pathlib by allowing a variant of glob, flob, to be
patched into the pathlib.Path class
"""
# Strictly speaking the only imports one should be doing here 
# is the pathlib one. All others should really be loaded from
# within the functions requiring the feature. This is to stop
# the overlay from triggering cyclic imports.
from six         import PY2, PY3
from fnmatch     import fnmatch
# from _pathlib_   import *
from pathlib     import *
from pathlib     import _windows_flavour, _posix_flavour, Path
from collections import namedtuple
from tempfile    import _get_default_tempdir as tempdir, _get_candidate_names as tempstr
from datetime    import datetime
if PY2 :
 from itertools  import groupby, izip_longest as lzip, chain
if PY3 :
 from itertools  import groupby, zip_longest as lzip, chain
 
# from platform import system
import os # No longer required (Well not entirely true try to work it out)

# Classes
# from namedlist import namedlist # NamedTuples are preferred now
# Folder = namedlist("Folder", ["id","path","files"])
# File   = namedlist("File", ["id","path","links"])

Folder = namedtuple("Folder", ["path","data"])
File   = namedtuple("File",   ["path","data"])

class MetaPath(type):
#  def __new__(cls, args) : 
 pass

class Path(Path) :

 _flavour = _windows_flavour if os.name == 'nt' else _posix_flavour
#  __slots__ = ("flob")

#  def __new__(cls, *args, **kwargs):
#   if cls is Path:
#       cls = WindowsPath if os.name == 'nt' else PosixPath
#   self = cls._from_parts(args, init=False)
#   if not self._flavour.is_supported:
#       raise NotImplementedError("cannot instantiate %r on your system"
#                                 % (cls.__name__,))
#   self._init()
#   return self 
#  
 
#  def __new__(cls, path, *args, **kvps):
#   # This allows one to process the None but I'm not sure if it messes with the flavour, only time and testing will tell.
#   print("__new__")
#   return super(Path, cls).__new__(cls, path or '.', *args, **kvps)

#  def __init__(self, path, *args, **kvps):
#   super().__init__(path or '.', *args, **kvps)
  
 def flob(self, glob = "**", depth = [], index = False, sorter = "stem based"):
  """
  Flob is a variant of glob that returns folders aswell, hence 
  the name. 
  
  The usual pattern matching is supported, that is the recursive 
  form '**' is supported. This is possible since glob, and flob, 
  use fnmatch under the hood. Similarly Path.(r)glob is really 
  glob under the hood. 
  
  flob returns the folder(s) and files in a sorted depth first
  search order.
  
  This is probably more closely related to os.walk but uses the
  pathlib Path API/Objects. 
  
  The output may be sorted by various means. The sorter key is
  used to select which method is used.
  
  ========== =============================================================================
  Sorter     Description
  ========== =============================================================================
  path based Prefers folders over files
  name based Sorts by path then prefering folders over files
  stem cased Sorts by file/folder name prefering folders over files
  stem based Sorts by file/folder name, irrespective of case, prefering folders over files
  ========== =============================================================================
     
  Arguments :
   root  : 
    This is the root path to start searching from
   glob  : 
    This is the file name pattern that matches the files
    in the folder.
   depth : 
    This is the tuple which comprises the index later on
   index : 
    returns an ordered tuple along with the path which 
    indicates file or folder order e.g. (3,2,5) is the 
    fifth file or folder in the second folder of the 
    third folder. 
           
  Note :
   Index is redundant and the user should wrap flob within an
   enumerate call instead if they require unique indices.
    
  Note :
   This is probably a bit redundant since rglob provides the 
   same functionality. I have included blob now which does a 
   depth first search and presents it as a breadth first one.
  """
  # This should become part of an overly for Pathlib. I noted 
  # previously that index and depth should be removed but I'm 
  # not sure why I said so. Also this provides the same set of
  # functionality as rglob.
  # It might prove useful to make hooks available for when one
  # enters or exits a directory during a traversal. Entry and 
  # exit hooks would be traversed as one entered and exited a
  # deep tree and a bouncing would occur a tree with many branches
  sort = {"path based": lambda p: (not p.is_dir(),  str(p)),  # Was using Path Based sorting
          "name based": lambda p: (str(p),  not p.is_dir()),
          "stem cased": lambda p: (str(p.stem), p.is_dir()),
          "stem based": lambda p: (str(p.stem).lower(), p.is_dir())}[sorter]
  for cntr, path in enumerate(sorted(self.iterdir(), key = sort)):
   if path.is_dir() :
    yield (path, tuple(depth + [cntr])) if index else path
    if glob.startswith("**"):
     yield from path.flob(glob, (*depth, cntr))
   elif fnmatch(path, glob) :
    yield (path, tuple(depth + [cntr])) if index else path # Originally  (*depth, cntr)


 def pathdiff(self, path) :
  """
  Iterates over the items in two lists grouping common elements
  in both until a differences is found, elements thereafter are
  grouped separately.
  
  Arguments :
   A : First list 
   B : Second list
  """
  C = 0
  share = []
  upper = []
  lower = []
  for c,p in enumerate(lzip(self.parts, path.parts)) :
   if p[0] == p[1] and C == c: 
    C += 1
    share.append(p[0])
   else : 
    if p[0] :
     upper.append(p[0])
    if p[1] :  
     lower.append(p[1])
  return (share, upper, lower)

 def walk(self, *args, glob = "**", test = lambda path : True, sorter = "path based", relative = False, **kvps) :
  """
  This generates a directory mapping for a given root. It uses 
  a globbing to identify the initial set of files of interest.
 
  One may then filter this further by specifying a test. 
  The test function is passed the file name and may return any
  non empty value for the file to be retained. Note that this
  value is cached/stored within the structure. The default test
  function is as follows 
  ::
  
      test = lambda filepath : True
      
  A really good use for test is for the exclusion of files by
  means of the ``fnmatch`` fucntion from the module with the 
  same name.
  ::
  
    from fnmatch import fnmatch
    test = lambda path : not any([fnmatch(path, excl) for excl in exclude])  
    
  To exclude specific files try the following which relies upon
  relative paths or use path stems to filter results.
  ::
  
    from fnmatch import fnmatch
    test = lambda path : not any([fnmatch(path.relative_to(root), excl) for excl in exclude])  
    
  The following could also be used but it appears more flaky.
  I've had it fail when there is only one element in esclude
  for instance.
  ::
  
    from functools import partial
    from itertools import chain
    from fnmatch import fnmatch
    test = lambda path : not any(chain(*map(partial(path), exclude)))
  
  Note :
   This function caches/stores the entire file tree under the
   current Path so do not run it on the internet or something.
  """
#   tier  = (lambda path : path.relative_to(self)) if relative else (lambda path : path)            # Alternative name : realte or route ?
  fifo  = lambda path : Folder(path, []) if path.is_dir() else File(path, test(path)) # Alternative name : packer or typer or asign ?
  stack = [Folder(self, [])]
  basis = self.parts
  for path in chain(self.glob(glob), [self]): # chain(self.flob(glob, sorter = "path based"), [self]) :
 #   print(path)
 #   data = {k:v for k,v in groupby(lzip(stack[-1][0].parts, path.parts), key=lambda p : "C"*(p[0]==p[1]) + "U"*(p[0]!=p[1] and None not in p) + "R"*(p[0] is None) + "L"*(p[1] is None))}
   trunk, older, newer = stack[-1][0].pathdiff(path)
#    print(trunk, older, newer)
   for item in older :
    node = stack.pop()
    if node[1] : 
     stack[-1][-1].append(Folder(node[0].relative_to(self) if relative else node[0], node[1]) if isinstance(node, Folder) else File(node[0].relative_to(self) if relative else node[0], node[1])) # This is a bit cludgy but it was the easiest means of returning relative paths
   # Here is where one might emit whole branches
 #   if trunk = basis :
 #    node = stack[-1][-1].pop(0)
   for item in newer :
    stack.append(fifo(stack[-1][0]/item))
  if len(stack) == 1 :
   return stack.pop()
  else :
   raise ValueError("If you are receiving a ValueError then there is a bug in the Pathlib Overlay, specifically in Path.walk the stack is not being reduced to a single element properly which is the purpose of chaining self with the flob generator")

 def blob(self, *args, glob = "**", test = lambda path : True, **kvps) :  
  """
  This iterator returns complete path traversals. It returns 
  both the parent folders and the files found. 
  
  Note :
   As this relies upon Path.walk it generates a full map of 
   the tree in memory.
  """
  def flatten(tree) :
   for node in tree[1] :
    if isinstance(node, Folder):
     yield node[0]
     yield from flatten(node)
    else : 
     yield node[0]
  yield from flatten(self.walk(self, *args, glob = glob, test = test, sorter = "path based", **kvps))

# def treedump(tree, dent = 0, printer = print) :
#  printer(" " * dent + "{}".format())
#  for item in tree[]
 @staticmethod
 def tmp() :
  """Temporary directory for the operating system
  
  This returns the current temporary directory for the system.
  This is determined by the :meth:`_get_default_tempdir` of the :mod:`tempfiles` package.
  """
  return Path(tempdir())

 @staticmethod
 def tmppath(tmpdir = None, prefix = None, suffix = None, format = "{prefix}{random}{suffix}", random = lambda : next(tempstr())) :
  """Generate a temporary file/folder path given an optional prefix, suffix and preferred path.
  
  :meth:`tmppath`, by default, generates a path from the systems' temporary directory with the prefix "tmp" and no suffix.
  The user may then create either a file or a folder from this path as necessary.
  
  The user may customize how this path is generated by setting the following arguments
  
  Arguments:
   tmpdir : 
     The root folder that will contain the temporary file [default : Path.tmp()]
   prefix : 
     Usually the name of the file [default : "tmp"]
   suffix :
     Usually the file extention, including the dot. [default : ""]
   random :
     A function that ensures some random character suitable for use in a file name.
     The random value function defaults to :meth:`_get_candidate_names` from :mod:`tempfile`.
   format :
     The forma for the file name. [Default : "{prefix}{random}{suffix}"]
    
  Note :
   Time stamps are only "random" to the nearest millisecond and should in general not be used to generate the random value unless one can guarnatee that a millisecond has ellapsed before each call.
   Should one wish to use a timestamp for the random value then the follow should preferably be used.
   ::
   
     lambda : datetime.datetime.now().strftime("%Y%m%dT%f")

  Example :
   One may readily generate a temporary file within a temporary directory as follows.
   ::
   
     Path.tmppath(Path.tmppath(), suffix=".tmp")
     
   More typically one would want to create the folder and the file accordingly.
   ::
   
     dir = Path.tmppath()
     dir.mkdirs()
     with open(Path.tmppath(dir, suffix=".tmp")) as file :
      # Populate the file
     dir.rmdir()
  """
  prefix = prefix or "tmp"
  suffix = suffix or ""
  tmpdir = tmpdir or Path.tmp()
  return Path.tmp()/format.format(prefix = prefix, random = random(), suffix = suffix)

 def temp(self, dir=False) :
  """Generate a temporary file from this file name"""
  if self.is_dir() :
   return 
  else :
   return self.parent/(self.stem + datetime.now().strftime(".%Y%m%dT%H%M%S") + self.extn)
#   Path(tmpdir)/(prefix+tempstr+suffix)
#   return Path.tmpFile(self.path, self.stem, self.extn, dir = self.is_dir())

if __name__ == "__main__" :
 from pprint import pprint
 pprint([p for p in Path(".").flob()])
