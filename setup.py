"""
Develop
-------

To setup a development installation one may call either pip as 
follows;
::
   pip install -e .
 
or execute the setup script directly;
::
   python setup.py develop

the pip version is generally preferred.

Document
--------

Documentation may be generated using the setup script.
::
   python setup.py build_sphinx -b singlehtml|html|latex

Select between singleHTML, HTMl and LaTeX as necessary.
The output is saved under :file:`dist/html|latex`.
"""
from setuptools import setup, find_packages
# import overlay
import os

setup(
 name                 = 'Apeman-Overlays',
 version              = ".".join([str(v) for v in (0,0,0)]), 
 description          = 'Overlays for Python',
 long_description     = open(os.path.join(os.path.dirname(__file__), 'README.rst')).read(),
 url                  = 'http://www.manaikan.com/',
 author               = 'Carel van Dam',
 author_email         = 'carelvdam@gmail.com',
 license              = 'PSFL',
# license              = 'Mannaikan Licence', # TODO : Specify (See below)
 platforms            = 'any',
 install_requires     = [# 'apeman',          # Minimal requirement : Uncomenting this hoever will break the ApeMan tox tests
                         'pathlib',
                         'docopt'],
#  scripts            = [],      # Command Line callable scripts e.g. '/Path/To/Script.py'
 packages             = find_packages(exclude=['original', ]), # Original is to be merges into either Overlay or Apeman as necessary
 include_package_data = True,
 zip_safe             = False,
 test_suite           = "tests",
#  tests_require      = ['tox'],
#  cmdclass           = {'test': Tox},
 command_options      = {'build_sphinx': {
                          'version'    : ('setup.py', "0.0"),
                          'release'    : ('setup.py', "0.0.0"),
                          'source_dir' : ('setup.py','docs'),
                          'build_dir'  : ('setup.py','dist'),
                          'config_dir' : ('setup.py','docs'),}}, 
 classifiers          = [ # See https://pypi.python.org/pypi?%3Aaction=list_classifiers
  "Development Status :: 3 - Alpha",
  "Environment :: Plugins",
  "Framework :: Apeman :: Overlay ",
  "Intended Audience :: Developers",
#  "License :: Free for non-commercial use", # TODO : Specify (See above)
#  "License :: Other/Proprietary License",
#  "License :: OSI Approved :: Python Software Foundation License",
  "Natural Language :: English",
  "Operating System :: OS Independent",
  "Programming Language :: Python",
  "Programming Language :: Python :: 2",
  "Programming Language :: Python :: 2.7",
  "Programming Language :: Python :: 3",
  "Programming Language :: Python :: 3.4",
  "Programming Language :: Python :: 3.5",
  "Programming Language :: Python :: 3.6",
  "Topic :: Software Development",
  "Topic :: Software Development :: Libraries",
  "Topic :: Software Development :: Libraries :: Python Modules",
  "Topic :: Software Development :: Libraries :: Patches",
#  "Topic :: Software Development :: Patches :: Overlays",  # Covered above
#  "Topic :: Software Development :: Localization",         # Not strictly applicable
#  "Topic :: Software Development :: User Interfaces",      # Not strictly applicable
#   "Topic :: Utilities",
 ],
)
