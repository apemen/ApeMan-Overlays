"""
Module Attributes
=================

These tests ensure that a patched module has the same attributes as a normal one.
"""
attributes = dir()
import sys
import overlays
from pprint        import pprint 
from unittest      import TestCase, main
from unittest.mock import patch

module = \
"""
"""

class testAttributes(TestCase) :
 """
 Test the attributes in a mock'd module are similar or equivalent to those in a normal module
 """

 @patch.module("module", "module.py", module)
 @patch("sys.modules", sys.modules.copy())
 def testAttributeSets(self, exclude = ["__cached__", "__annotations__"]):
  init = set(sys.modules.keys())
  import module
  #  pprint(dir(module))
  term = set(sys.modules.keys())
  answer = {'module'}
  result = set(term) - set(init)
  self.assertEqual(answer, result)
  #  print(term-init)
  answer = [item for item in  attributes if item not in exclude]
  result = dir(module)
  self.assertEqual(answer, result)

if __name__ == "__main__" :
 main()
#  init = set(sys.modules.keys())
#  exclude = ['main', 'mock_module', 'module', 'patch', 'sys', 'pprint','exclude', 'init', 'term']
#  term = set(sys.modules.keys())
# #  print(term-init)
#  pprint(set([item for item in dir() if item not in exclude]) - set(main()))
